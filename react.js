module.exports = {
  env: {
    browser: true,
    jest: true
  },
  extends: ["plugin:react/recommended", "airbnb", "prettier"],
  parser: "babel-eslint",
  plugins: [
    "react",
    "react-hooks"
  ],
  rules: {
    "import/extensions": [0],
    "import/first": [0],
    "import/no-default-export": 1,
    "import/prefer-default-export": [0],
    "jsx-a11y/href-no-hash": [0], // deprecated rule but dependencies required this line or else errors
    "jsx-a11y/label-has-for": [0],
    "jsx-a11y/no-static-element-interactions": [0],
    "jsx-a11y/no-noninteractive-element-interactions": [0],
    "jsx-a11y/click-events-have-key-events": [0], // ehh not sure about this but it's annoying, https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/click-events-have-key-events.md
    "no-return-assign": [0], // redux reducers use return-assignment
    "react/no-typos": [0], // eslint thinks our variable names are typos
    "react/no-unescaped-entities": [0],
    "react/jsx-filename-extension": [
      "error",
      { "extensions": [".jsx", ".tsx"] }
    ],
    semi: [2, "never"]
  }
};
