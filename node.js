module.exports = {
  env: {
    node: true
  },
  globals: {},
  extends: ["airbnb", "prettier"],
  rules: {
    "no-underscore-dangle": [0],
    semi: [2, "never"]
  }
};
