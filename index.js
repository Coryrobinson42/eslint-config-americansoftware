module.exports = {
  env: {
    browser: true,
    node: true,
    jest: true
  },
  globals: {},
  extends: ["airbnb", "prettier"],
  rules: {
    "import/extensions": [0],
    "import/first": [0],
    "import/no-extraneous-dependencies": [0],
    "import/no-unresolved": [0],
    "import/prefer-default-export": [0],
    "no-underscore-dangle": [0],
    semi: [2, "never"]
  }
};
